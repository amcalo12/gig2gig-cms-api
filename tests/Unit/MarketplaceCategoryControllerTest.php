<?php

namespace Tests\Unit;

use App\Models\Marketplace;
use App\Models\MarketplaceCategory;
use Tests\TestCase;

use App\Models\User;
use App\Models\UserDetails;

class MarketplaceCategoryControllerTest extends TestCase
{
    protected $token;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $user = factory(User::class)->create([
                'email' => 'token@test.com',
                'password' => bcrypt('123456')]
        );
        $this->testId = $user->id;
        $user->image()->create(['url' => $this->faker->url,'name'=>'test']);
        $userDetails = factory(UserDetails::class)->create([
            'type'=>2,
            'user_id' => $user->id,
        ]);
        $response = $this->post('api/login', [
            'email' => 'token@test.com',
            'password' => '123456',
        ]);

        $this->token = $response->json('access_token');
    }


    public function test_all_marketplace_category_200()
    {
        $marketplaceCategory = factory(MarketplaceCategory::class, 5)->create();

        $market = factory(Marketplace::class)->create([
            'featured'=>'yes',
            'marketplace_category_id'=>MarketplaceCategory::all()->random()->id
        ]);

        $market->image()->create([
            'name'=>'test',
            'url'=>$this->faker->url,
        ]);

        $response = $this->json('GET',
            'api/a/marketplace_categories'. '?token=' . $this->token);

        $response->assertStatus(200);

    }

}


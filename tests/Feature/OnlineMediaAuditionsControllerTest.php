<?php

namespace Tests\Feature;

use App\Models\Appointments;
use App\Models\Auditions;
use App\Models\OnlineMediaAudition;
use App\Models\User;
use App\Models\UserDetails;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OnlineMediaAuditionsControllerTest extends TestCase
{
    protected $token;
    protected $testId;


    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $user = factory(User::class)->create([
                'email' => 'token@test.com',
                'password' => bcrypt('123456')]
        );
        $this->testId = $user->id;
        $user->image()->create(['url' => $this->faker->url, 'name' => 'test']);
        $userDetails = factory(UserDetails::class)->create([
            'type' => 2,
            'user_id' => $user->id,
        ]);
        $response = $this->post('api/login', [
            'email' => 'token@test.com',
            'password' => '123456',
        ]);

        $this->token = $response->json('access_token');

    }


    public function test_add_media_online_submision()
    {
        $audition = factory(Auditions::class)->create(['user_id' => factory(User::class)->create()->id]);
        $data = [
            'performer_id' => $this->testId,
            'appointment_id' => factory(Appointments::class)->create(['auditions_id' => $audition])->id,
            'type'=>'video',
            'url'=>'http://test/video/user.mp4',
            'thumbnail'=>'http://test/video/user.png',
            'name'=>'audition-test'
        ];

        $response = $this->json('POST','api/a/media/online?token='.$this->token,$data);
        $response->assertStatus(201);
        $response->assertJsonStructure(
            [
                'message',
                'data'=>[
                    'id',
                    'type',
                    'url',
                    'name',
                    'appointment_id',
                    'performer_id'
                ]
            ]
        );
    }

    public function test_list_by_user_media_online_submision()
    {
        $audition = factory(Auditions::class)->create(['user_id' => factory(User::class)->create()->id]);
        $appointment = factory(Appointments::class)->create(['auditions_id'=>$audition->id]);
        $user = factory(User::class)->create();
        factory(OnlineMediaAudition::class,5)->create([
            'performer_id'=>$user->id,
            'appointment_id'=>$appointment->id,
            'type'=>$this->faker->randomElement(['video','doc'])
        ]);

        $data= [
            'performer_id'=>$user->id,
            'appointment_id'=>$appointment->id
        ];

        $response = $this->json('GET','api/media/online?token='.$this->token,$data);
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [

                'data'=>[[
                    'id',
                    'type',
                    'url',
                    'name',
                    'appointment_id',
                    'performer_id'
                ]]
            ]
        );
    }

    public function test_list_by_round_media_online_submision()
    {
        $audition = factory(Auditions::class)->create(['user_id' => factory(User::class)->create()->id]);
        $appointment = factory(Appointments::class)->create(['auditions_id'=>$audition->id]);
        $user = factory(User::class)->create();
        factory(OnlineMediaAudition::class,5)->create([
            'performer_id'=>$user->id,
            'appointment_id'=>$appointment->id,
            'type'=>$this->faker->randomElement(['video','doc'])
        ]);



        $response = $this->json('GET','api/media/online/'.$appointment->id.'/round?token='.$this->token);
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [

                'data'=>[[
                    'id',
                    'type',
                    'url',
                    'name',
                    'appointment_id',
                    'performer_id'
                ]]
            ]
        );
    }
    public function test_list_by_id_media_online_submision()
    {
        $audition = factory(Auditions::class)->create(['user_id' => factory(User::class)->create()->id]);
        $appointment = factory(Appointments::class)->create(['auditions_id'=>$audition->id]);
        $user = factory(User::class)->create();
        $media = factory(OnlineMediaAudition::class)->create([
            'performer_id'=>$user->id,
            'appointment_id'=>$appointment->id,
            'type'=>$this->faker->randomElement(['video','doc'])
        ]);



        $response = $this->json('GET','api/media/online/'.$media->id.'?token='.$this->token);
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [

                'data'=>[
                    'id',
                    'type',
                    'url',
                    'name',
                    'appointment_id',
                    'performer_id'
                ]
            ]
        );
    }
}

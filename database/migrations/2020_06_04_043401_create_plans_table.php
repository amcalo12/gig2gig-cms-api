<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('header')->nullable();
            $table->string('stripe_plan')->nullable();
            $table->integer('allowed_performers')->default(0);
            $table->text('description')->description();
            $table->float('amount')->nullable()->default(0);
            $table->enum('type',['monthly','annual','quarterly','daily'])->nullable()->default('monthly');
            $table->integer('user_type')->nullable();
            $table->boolean('is_custom')->default(false);
            $table->boolean('is_discounted')->default(false);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}

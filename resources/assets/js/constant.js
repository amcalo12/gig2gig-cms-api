const DEFINE = {
    no_img_placeholder: "/images/default.jpg",
    user_default_img: "/images/default-avatar.jpg",
    user_type : {1 : 'Casting' , 2 : 'Performer'}
}

export default DEFINE;
export default {
  search: state => term => state.topics.search(term)
};

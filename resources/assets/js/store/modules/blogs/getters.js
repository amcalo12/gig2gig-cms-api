export default {
  search: state => term => state.blogs.search(term)
};

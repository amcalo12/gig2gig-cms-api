export default {
  search: state => term => state.plans.search(term)
};

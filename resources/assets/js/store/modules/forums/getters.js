export default {
  search: state => term => state.forums.search(term)
};

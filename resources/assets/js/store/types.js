export const TOGGLE_SPINNER = '⏲ Toggle Spinner';

// auth.js
export const LOGOUT = '🚪 Logout';
export const SAVE_TOKEN = '🔒 Save Token';
export const FETCH_USER_SUCCESS = '👱✅ Fetch User Successful';
export const FETCH_USER_FAILURE = '👱❌ Fetch User Failed';

export const FETCH_AUDITIONS_SUCCESS = 'Fetch Auditions successful';
export const FETCH_AUDITIONS_FAILURE = 'Fetch Auditions failed';
export const DELETE_AUDITION = 'Delete Audition';
export const ACCEPT_BAN = 'Accept Ban Audition';
export const REMOVE_BAN = 'Remove Ban Audition';

export const FETCH_CONTRIBUTORS_SUCCESS = 'Fetch Contributors successful';
export const FETCH_CONTRIBUTORS_FAILURE = 'Fetch Contributors failed';
export const DELETE_CONTRIBUTOR = 'Delete Contributor';

export const FETCH_PERFORMERS_SUCCESS = 'Fetch Performers successful';
export const FETCH_PERFORMERS_FAILURE = 'Fetch Performers failed';
export const DELETE_PERFORMER = 'Delete Performer';

export const FETCH_PRODUCTION_TYPES_SUCCESS = 'Fetch Production Types successful';
export const FETCH_PRODUCTION_TYPES_FAILURE = 'Fetch Production Types failed';
export const UPDATE_PRODUCTION_TYPE = 'Update Production Type';
export const DELETE_PRODUCTION_TYPE = 'Delete Production Type';

export const FETCH_SKILLS_SUCCESS = 'Fetch Skills successful';
export const FETCH_SKILLS_FAILURE = 'Fetch Skills failed';
export const CREATE_SKILL = 'Create Skill';
export const UPDATE_SKILL = 'Update Skill';
export const DELETE_SKILL = 'Delete Skill';

export const FETCH_CATEGORIES_SUCCESS = 'Fetch Categories successful';
export const FETCH_CATEGORIES_FAILURE = 'Fetch Categories failed';
export const CREATE_CATEGORY = 'Create Category';
export const UPDATE_CATEGORY = 'Update Category';
export const DELETE_CATEGORY = 'Delete Category';

export const FETCH_TOPICS_SUCCESS = 'Fetch Topics successful';
export const FETCH_TOPICS_FAILURE = 'Fetch Topics failed';
export const CREATE_TOPIC = 'Create Topic';
export const UPDATE_TOPIC = 'Update Topic';
export const DELETE_TOPIC = 'Delete Topic';

export const FETCH_BLOGS_SUCCESS = 'Fetch Blogs successful';
export const FETCH_BLOGS_FAILURE = 'Fetch Blogs failed';
export const CREATE_BLOG = 'Create Blog';
export const UPDATE_BLOG = 'Update Blog';
export const DELETE_BLOG = 'Delete Blog';


export const FETCH_FORUMS_SUCCESS = 'Fetch Forums successful';
export const FETCH_FORUMS_FAILURE = 'Fetch Forums failed';
export const CREATE_FORUM = 'Create Forum';
export const UPDATE_FORUM = 'Update Forum';
export const DELETE_FORUM = 'Delete Forum';



export const FETCH_VENDORS_SUCCESS = 'Fetch Vendors successful';
export const FETCH_VENDORS_FAILURE = 'Fetch Vendors failed';
export const CREATE_VENDOR = 'Create Vendor';
export const UPDATE_VENDOR = 'Update Vendor';
export const DELETE_VENDOR = 'Delete Vendor';

export const FETCH_SUBSCRIPTIONS_SUCCESS = 'Fetch Subscriptions successful';
export const FETCH_SUBSCRIPTIONS_FAILURE = 'Fetch Subscriptions failed';
export const UPDATE_SUBSCRIPTION = 'Update Subscription';

// Payments
export const FETCH_PAYMENTS_SUCCESS = 'Fetch Payments Successful';
export const FETCH_PAYMENTS_FAILURE = 'Fetch Payments Failed';

// user
export const FETCH_USERS_SUCCESS = 'Fetch Users successful';
export const FETCH_USERS_FAILURE = 'Fetch Users failed';
export const UPDATE_USER = 'Update User';
export const DELETE_USER = 'Delete User';
export const USER_STATUS_CHANGE_SUCCESS = 'User status change successful';

// Payments
export const FETCH_PLANS_SUCCESS = 'Fetch Plans Successful';
export const FETCH_PLANS_FAILURE = 'Fetch Plans Failed';
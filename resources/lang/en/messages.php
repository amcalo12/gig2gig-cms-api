<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Response Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by API Responses.
    |
    */
    // General
    'data_not_found' => 'Data Not Found',
    'error' => 'ERROR',
    'success' => 'Success',
    'not_processable' => 'Not processable',
    'unprocesable_entity' => 'Unprocessable Entity',
    'data_not_update' => 'Data Not Update',
    'custom_unauthorized' => 'This user is already registered, please try again or reset your password',
    'unauthorized' => 'Unauthorized',
    'server_error' => 'Server Error',
    'something_went_wrong' => 'Something went wrong',
    'account_deactivated' => 'Sorry! your account has been deactivated by administration',

    // Feedback Controller
    'feedback_not_add' => 'Feedback not add',
    'feedback_not_update' => 'Feedback not update',
    'comment_added' => 'Comment added successfully',
    'comment_not_added' => 'Something went wrong! Comment not added',
    'already_kept_future' => 'Performer already added to keep for future list',

    // Aparences Controller
    'error_not_processable' => 'Error not Processable',

    // Appointments Auditions Controller
    'appointment_not_assigned' => 'Appointment not assigned',

    'slot_reserved' => 'Slot reserved successfully',
    'slot_vacant' => 'Slot emptied successfully',

    // Appointment Controller
    'round_closed_successfully' => 'Round closed successfully',
    'round_opened_already' => 'This round is already active',
    'round_reopened' => 'Round re-opened successfully',
    'round_not_reopened' => 'Round could not be re-opened',
    'round_not_close' => 'Round not close',
    'list_by_slots' => 'List by slots',
    'round_create' => 'Round Create',
    'round_not_create' => 'Round not Create',
    'no_slot_available' => 'No Slot Available',

    // Audition Management Controller
    'audition_not_update' => 'Audition not update',
    'error_to_open_audition' => 'error to open audition',
    'error_to_close_audition' => 'error to close audition',
    'audition_banned' => 'Audition Banned',
    'you_already_registered' => 'You already registered',
    'audition_saved' => 'Audition Saved',
    'group_not_creaed' => 'Group not Created',
    'group_creaed' => 'Group Created',
    'group_already_open' => 'Group already Open',
    'group_already_close' => 'Group already Closed',
    'group_open' => 'Group is Open',
    'group_close' => 'Group is Close',
    'group_close_success' => 'Group Closed successfully',
    'group_not_closed' => 'Group not Closed',
    'video_saved' => 'Video saved',
    'video_not_saved' => 'Video not saved',
    'user_already_uploaded_video' => 'User :user, have already uploaded Video',
    'already_uploaded_video' => 'User already uploaded Video',
    'assign_number_created' => 'Assign number Created Successfully',
    'assign_number_not_created' => 'Failed to Create Assign Number',
    'number_already_assigned' => 'Number Already Assigned',
    'number_already_used' => 'Number already Used, Change the number',
    'to_upcomming_not_allowed' => 'Sorry! Moving to upcoming is not allowed for this request',
    'save_audition_not_allowed' => 'Sorry! Saving this audition is not allowed without Manager',

    // Auditions Controller
    'contruibuitors_add' => 'Contributors Add',

    // Auth Controller
    'successfully_logged_out' => 'Successfully logged out',

    // Calender Controller
    'event_deleted' => 'Event deleted',
    'cant_use_past_dates' => "Can't use past dates",
    'end_date_must_be_greater_than_start_date' => "End date must be greater than start date",
    'date_range_is_occupied' => "Date range is occupied",
    'error_process_event' => 'Error process event',


    // Feedback
    'feedback_save_success' => 'Feedback saved successfully',
    // Final Cast Controller
    'fail_to_add_performer' => 'fail to add performer',
    'add_performer_to_final_cast' => 'Add performer to final cast',
    'performer_restored_success' => 'Performer restored successfully',
    'performer_restored_failure' => 'Performer can not be restored',

    // Market Place Controller
    'error_created_marketplace' => 'Error created Marketplace',

    // MarketplaceFeaturedListingController
    'error_created_marketplace_featured_listing' => 'Error created Marketplace Featured Listing',

    // Monitor manager controller
    'update_not_publised' => 'Update Not Published',

    // Notification Controller
    'record_not_created' => 'record_not_created',
    'notifications_not_found' => 'There are no notifications',

    // OnlineMediaAuditionController
    'media_created' => 'Media created',
    'media_not_created' => 'Media not created',
    'media_not_found' => 'Media not found',

    // PerformersController
    'error_add_performer' => 'Error add performer',
    'code_share' => 'Code share',
    'error_send_code' => 'Error Send Code',
    'tag_by_user' => 'Tag by User',
    'comment_by_user' => 'Comment by User',
    'contracts_by_user' => 'contracts by user',

    // UserController
    'user_deleted' => 'User Deleted',
    'email_not_found' => 'Email not Found',
    'unions_update' => 'Unions Update',

    // Users Settying controller
    'setting_updated' => 'Setting Updated',
    'setting_not_updated' => 'Setting Not Updated',

    'default_instant_feedback_message' => "Thanks for attending. That's all we need today",

    'subscribe_success' => "Subscription created successfully",
    'subscribed_already' => "Sorry! You already have active subscription",
    'subscribe_failed' => "Sorry! We couldn't subscribe you to this plan, try again later",
    'subscribe_cancelled' => 'Your subscription has been cancelled successfully',
    'subscribe_resumed' => 'Your subscription has been resumed successfully',

    'change_payment_success' => "Payment method changed successfully",

    'online_audition_past' => "The audition media submission has ended",

    'add_to_talent_message' => "Add this performer to Talent Database for sharing profile",
 
    'caster_notification_success' => "Notifications sent successfully",
    'resend_email_success' => "Invitation email sent successfully",
    'is_already_admin' => ""
    // Market Place Categories Controller
    // Managers Controller
    // NotificationManagementController
    // Audition videos Controller
    // Comments Controller
    // Content Setting Controller
    // Controller
    // Credits Controller
    // Educations Controller
];

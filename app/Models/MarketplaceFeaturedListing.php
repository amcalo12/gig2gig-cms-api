<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketplaceFeaturedListing extends Model
{
    protected $fillable = ['business_name', 'email'];

}
